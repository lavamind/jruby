# JRuby in Debian

## Building the package

To build this package, it's highly recommended to use a clean build environment
such as that provided by sbuild. The build process dumps a lot of artifacts and
temporary files in the source tree that are difficult to track and cleanup
properly.

The first step after obtaining the source and setting up a build environment is
getting the orig-source tarballs:

    gbp export-orig

The Debian source tree should be ready to build.

### A note on bundled Ruby libraries

JRuby as a project does not track the latest versions of C Ruby. As new C Ruby
versions are released, JRuby inevitably falls behind. For example, JRuby 9.3.x
officially declares Ruby 2.6 compatibility, but this version of Ruby has long
been removed from Debian stable and replaced by newer versions.

In order to ensure JRuby works reliably in Debian, we therefore bundle a number
of versioned gems obtained from rubygems.org into an orig-source tarball, in
addition to the few Ruby libraries still bundled directly in the upstream source
tree.

The required gems are sourced from the `default_gems` list in `lib/pom.rb`.

## Build process

The build relies on polyglot-ruby, which transforms `pom.rb` files into XML at
build-time for Maven to consume. Since the `debian-maven-helper` tools are not
knowlegeable about `pom.rb` files, a `debian/jruby.poms` cannot be used.

Therefore, plugin removals and debianizing versions is done by directly patching
`pom.rb` files. Also, `${maven:*}` substvars are not generated.

## Artifacts

The build process builds a jruby-core-*.jar artifact, consisting of the JRuby
code in addition to shaded dependencies, also known as a "fat" jar or uberjar.
This is the code executed when `/usr/bin/jruby` is invoked.

This is not the usual method of building Java packages in Debian and it's
perhaps possible to fix this, however this would need further patches to the
POM files and additional build process tweaks, see #789485 for a discussion.

## Tests

JRuby comes with an large array of unit and integration testsuites.
Unfortunately because our build and packaging processes are vastly different
from upstream, some test failures do occur in some of the testsuites.

Helpful information about debugging tests is provided by upstream at:
https://github.com/jruby/jruby/blob/master/BUILDING.md
