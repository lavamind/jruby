#!/bin/sh

. "$(dirname $0)/common.sh"

prepare

cd "$AUTOPKGTEST_TMP"

if [ "$(id -u)" -eq 0 ]; then
    # prevent chroot spec from running, chroot is not supported with jruby
    sed -i '/it_behaves_like :dir_chroot_as_root, :chroot/d' spec/ruby/core/dir/chroot_spec.rb
fi

for n in SC_CHILD_MAX SC_TZNAME_MAX SC_SYMLOOP_MAX; do
    echo "fails:Etc.sysconf returns the value of POSIX.1 system configuration variable ${n}" >> spec/tags/ruby/library/etc/sysconf_tags.txt
done

# skip failing tests on specific architectures
if [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armel" ] || \
   [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armhf" ]; then
    spec_skip ALL spec/ruby/core/file/lutime_spec.rb
    spec_skip ALL spec/ruby/core/file/utime_spec.rb
    spec_skip "returns the modification Time of the file" spec/ruby/core/file/mtime_spec.rb
    spec_skip "returns the modification Time of the file with microseconds" spec/ruby/core/file/mtime_spec.rb
    spec_skip "returns the last access time for the named file with microseconds" spec/ruby/core/file/atime_spec.rb
    spec_skip "is able to compare files by different modification times" spec/ruby/core/file/stat/comparison_spec.rb
    spec_skip "accepts a length, and an offset" spec/ruby/core/io/pread_spec.rb
    spec_skip "accepts a length, an offset, and an output buffer" spec/ruby/core/io/pread_spec.rb
    spec_skip "does not advance the file pointer" spec/ruby/core/io/pread_spec.rb
    spec_skip "raises EOFError if end-of-file is reached" spec/ruby/core/io/pread_spec.rb
    spec_skip "returns the number of bytes written" spec/ruby/core/io/pwrite_spec.rb
    spec_skip "accepts a string and an offset"  spec/ruby/core/io/pwrite_spec.rb
    spec_skip "does not advance the pointer in the file" spec/ruby/core/io/pwrite_spec.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "s390x" ]; then
    spec_skip ALL spec/ruby/core/array/pack/shared/float.rb
    spec_skip ALL spec/ruby/core/file/lutime_spec.rb
    spec_skip ALL spec/ruby/core/file/stat/comparison_spec.rb
    spec_skip "returns the last access time for the named file with microseconds" spec/ruby/core/file/atime_spec.rb
    spec_skip "returns the change time for the named file.*with microseconds\." spec/ruby/core/file/ctime_spec.rb
    spec_skip "returns the modification Time of the file with microseconds" spec/ruby/core/file/mtime_spec.rb
    spec_skip "may set nanosecond precision" spec/ruby/core/file/utime_spec.rb
    spec_skip "decodes NaN" spec/ruby/core/string/unpack/shared/float.rb
fi

# run the rspec testsuite
jruby -S rake spec:ruby:fast
