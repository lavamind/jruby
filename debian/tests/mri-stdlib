#!/bin/sh

. "$(dirname $0)/common.sh"

prepare
debug_tests

cd "$AUTOPKGTEST_TMP"

# exclude failing tests

# Unreliable tests relying on bespoke threading
# see https://github.com/jruby/jruby/issues/8118
mri_exclude test_validation_success TestOpenURISSL.rb
mri_exclude test_validation_failure TestOpenURISSL.rb
mri_exclude test_validation_noverify TestOpenURISSL.rb

# Causes the test to hang/timeout for an unknown reason
mri_exclude test_get__break TestNetHTTP_v1_2.rb

# Frequent timeout in salsa autopkgtest
mri_exclude test_interrupt_in_other_thread TestRelineAsReadline.rb

# skip failing tests on specific architectures
if [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "arm64" ]; then
    mri_exclude test_failed_path TestIO_Console.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armel" ] || \
     [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armhf" ]; then
    mri_exclude test_cp TestFileUtils.rb
    mri_exclude test_cp_r TestFileUtils.rb
    mri_exclude test_uptodate\? TestFileUtils.rb
    mri_exclude test_install TestFileUtils.rb
    mri_exclude test_failed_path TestIO_Console.rb
    mri_exclude test_utime TestPathname.rb
    mri_exclude test_asset TestRDocServlet.rb
    mri_exclude test_if_modified_since_not_modified TestRDocServlet.rb
    mri_exclude test_if_modified_since TestRDocServlet.rb
    mri_exclude test_generate_search_index_with_reproducible_builds TestRDocGeneratorJsonIndex.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "ppc64el" ]; then
    mri_exclude test_cooked TestIO_Console.rb
    mri_exclude test_raw TestIO_Console.rb
    mri_exclude test_raw_minchar TestIO_Console.rb
    mri_exclude test_raw_timeout TestIO_Console.rb
    mri_exclude test_raw_intr TestIO_Console.rb
    mri_exclude test_winsize TestIO_Console.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "s390x" ]; then
    mri_exclude test_failed_path TestIO_Console.rb
    # work around s390x File.utime() issue
    # reported upstream at https://github.com/jruby/jruby/issues/8043
    mri_exclude test_utime TestPathname.rb
    mri_exclude test_uptodate\? TestFileUtils.rb
    mri_exclude test_generate_search_index_with_reproducible_builds TestRDocGeneratorJsonIndex.rb
    mri_exclude test_shifting_midnight TestLogDevice.rb
    mri_exclude test_shifting_midnight_exist_file TestLogDevice.rb
fi

# run the rspec testsuite
jruby -S rake test:mri:stdlib
