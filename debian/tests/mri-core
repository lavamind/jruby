#!/bin/sh

. "$(dirname $0)/common.sh"

prepare
debug_tests

cd "$AUTOPKGTEST_TMP"

# exclude failing testcase
# with "Expected /OK/ (oid=4000) to be the same as /OK/ (oid=4004)."
# needs further investigation
mri_exclude test_regexp TestBasicInstructions.rb

# exclude unreliable signal+thread tests
# https://github.com/jruby/jruby/issues/4956
mri_exclude test_thread_join_in_trap TestThread.rb
mri_exclude test_thread_value_in_trap TestThread.rb

# flaky test
mri_exclude test_autoload_same_file_with_raise TestAutoload.rb

# skip failing tests on specific architectures
if [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armel" ] || \
     [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armhf" ]; then
    mri_exclude test_lutime TestFileExhaustive.rb
    mri_exclude test_utime TestFileExhaustive.rb
    mri_exclude test_utime_symlinkfile TestFileExhaustive.rb
    mri_exclude test_utime TestFile.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "i386" ]; then
    mri_exclude test_advise TestIO.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "ppc64el" ]; then
    mri_exclude test_autoclose TestIO.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "s390x" ]; then
    mri_exclude test_pack TestArray.rb
    mri_exclude test_pack TestPack.rb
    mri_exclude test_pack_unpack_fdeEgG TestPack.rb
    # work around s390x File.utime() issue
    # reported upstream at https://github.com/jruby/jruby/issues/8043
    mri_exclude test_utime TestFile.rb
    mri_exclude test_lutime TestFileExhaustive.rb
    mri_exclude test_utime TestFileExhaustive.rb
    mri_exclude test_utime_symlinkfile TestFileExhaustive.rb
fi

# run a single test
#jruby test/mri/runner.rb --excludes=test/mri/excludes:test/mri/excludes_wip test/mri/ruby/test_thread.rb

# run the full rspec testsuite
jruby -S rake test:mri:core
