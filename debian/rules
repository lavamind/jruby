#!/usr/bin/make -f

include /usr/share/dpkg/default.mk
include /usr/share/javahelper/java-vars.mk

export JAVA_HOME=/usr/lib/jvm/default-java

%:
	dh $@ --buildsystem=maven

override_dh_auto_clean:
	dh_auto_clean -- -Pclean
	rm -f debian/jruby.lintian-overrides
	rm -rf .pc_gems
	# cleanup bin executables
	cd $(CURDIR)/bin && ( \
		rm -f bundle bundler erb irb lock_jars racc rdoc ri ; \
	)
	# cleanup stdlib
	-cd $(CURDIR)/lib/ruby/stdlib && ( \
		rm -f $(foreach f,English abbrev ant base64 cmath debug delegate fileutils find getoptlong* ipaddr jar-dependencies jar_dependencies jar_install* kconv mutex_m nkf observer open-uri optionparser ostruct pp prettyprint pstore resolv-replace rinda ring tuplespace ruby2_keywords rubygems securerandom shellwords singleton stringio strscan time timeout tsort un weakref,$(f).rb) ; \
		rm -f $(foreach j,nkf stringio strscan,${j}.jar) ; \
		rm -rf benchmark* bigdecimal* bundler* cgi* csv* did_you_mean* digest* drb* erb* error_highlight* ffi* forwardable* io/console* io/wait* irb* jars jline jopenssl* json* libfixposix* logger* net/http* net/protocol* open3* openssl* optparse* org psych* racc* rake* random* rdoc* readline* reline* rinda* subspawn* scanf* uri* yaml* ; \
	    find rubygems -mindepth 1 -maxdepth 1 -not -name defaults -exec rm -rf {} \; \
	)
	# cleanup main builddirs
	rm -rf core/src/main/java/META-INF
	rm -rf lib/ruby/gems lib/jruby.jar
	rm -f shaded/dependency-reduced-pom.xml test/pom.xml
	-$(foreach p,. lib core shaded,test -f ${p}/pom.xml.bak && mv ${p}/pom.xml.bak ${p}/pom.xml;)
	# cleanup maven artifacts
	rm -f maven/pom.xml maven/jruby/pom.xml maven/jruby-complete/pom.xml
	rm -rf maven/jruby-complete/src/it/osgi_many_bundles_with_embedded_gems_*
	rm -rf maven/jruby-dist/target

execute_after_dh_auto_configure:
	# move stdlib rubygems into maven repository
	mv rubygems debian/maven-repo
	# backup upstream generated poms
	$(foreach p,. lib core shaded,cp ${p}/pom.xml ${p}/pom.xml.bak;)

override_dh_auto_build:
	# ensure org.jruby.maven in local maven repo is writeable
	# this is needed so that a given version of jruby can build itself
	mkdir debian/maven-repo/org/jruby.tmp
	cp -L -R debian/maven-repo/org/jruby/* debian/maven-repo/org/jruby.tmp/
	rm -rf debian/maven-repo/org/jruby
	mv debian/maven-repo/org/jruby.tmp debian/maven-repo/org/jruby
	# build selected profiles
	dh_auto_build -- -Djruby.revision=$(DEB_VERSION) -Pmain,complete,dist

override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	# java tests
	# to debug a single test, add -Dsurefire.useFile=false -Dtest=org/jruby/test/<TestFileName>.java
	dh_auto_test -- -Djruby.revision=$(DEB_VERSION) -Ptest -pl org.jruby:jruby-base
endif

DIST_TARBALL=./maven/jruby-dist/target/jruby-dist-$(DEB_VERSION_UPSTREAM:+ds=)-bin.tar.gz
DIST_TARGET_DIR=./target/package/
override_dh_prep:
	dh_prep
	mkdir -p $(DIST_TARGET_DIR)
	# unpack dist tarball in package target directory
	tar zxf $(DIST_TARBALL) -C $(DIST_TARGET_DIR) --strip-components=1
	# apply patches to bundled stdlib gems
	QUILT_PATCHES=debian/patches/gems QUILT_PC=.pc_gems quilt push -a
	# remove superfluous shell scripts
	rm -f $(DIST_TARGET_DIR)/bin/*.bat
	rm -f $(DIST_TARGET_DIR)/bin/*.sh
	# remove env from shell scripts
	sed -i '1s|^#!/usr/bin/env\sruby|#!/usr/bin/jruby|' $(DIST_TARGET_DIR)/bin/*
	# remove bundled libfixposix libraries
	find $(DIST_TARGET_DIR)/lib/ruby/stdlib/libfixposix/binary -type f -and -not -name version.rb -delete
	find $(DIST_TARGET_DIR)/lib/ruby/stdlib/libfixposix/binary -type d -and -empty -delete
	# remove bundled font files from stdlib (rdoc templates)
	# only remove the fonts that debian provides in packages
	find $(DIST_TARGET_DIR)/lib/ruby/stdlib -name Lato-\*.ttf -type f -print -delete
	# remove bundled jars available in debian
	rm -rf $(DIST_TARGET_DIR)/lib/ruby/stdlib/jline
	# remove unneeded executable bits from gem.home scripts
	find $(DIST_TARGET_DIR)/lib/ruby/gems/shared/gems/* -type f -executable \
		| while read script; do \
			sed -i '1,1{/^#\!\/usr\/bin\/env ruby/d}' $$script; \
			chmod -x $$script; \
		done

override_dh_auto_install:
	# don't ship jruby-stdlib maven artifact
	dh_auto_install --destdir=debian/jruby/ -O--buildsystem=maven -- -pl "!lib"

execute_after_dh_install:
	# clean and install polyglot-generated poms
	for p in maven/pom.xml maven/jruby/pom.xml; do \
		mh_cleanpom --keep-pom-version -pjruby $$p; \
		mh_installpom -pjruby $$p; \
	done
	# install org.jruby:jruby proxy jar
	mh_installjar -pjruby ./maven/jruby/pom.xml \
	        ./maven/jruby/target/jruby-$(DEB_VERSION_UPSTREAM:+ds=).jar
	# apply Class-Path attribute to main jar
	jh_classpath

execute_after_dh_installexamples:
	# remove vcs control file
	rm debian/jruby/usr/share/doc/jruby/examples/jnlp/.gitignore

execute_before_dh_lintian:
	sed 's,@DEB_VERSION_UPSTREAM@,$(DEB_VERSION_UPSTREAM:+ds=),g' \
		debian/jruby.lintian-overrides.in > debian/jruby.lintian-overrides
